---
title: "Ajouter une actualité sur Rzine"
author: <img src="figure/Bandeau_rzine_small.png" width="90">
date: "`r Sys.Date()`"
css: styles.css
output:
  rmdformats::readthedown
---

```{r, setup, eval=TRUE, include=FALSE}
knitr::opts_chunk$set(message=FALSE, warning=FALSE)


```

> Rzine vous permet de diffuser des billets d'actualité sur la pratique de R en Sciences Sociales. Ce document présente les instructions à suivre pour ajouter une actualité sur la plateforme.   

<br>

## Introduction

L'ajout d'actualité sur Rzine fonctionne de la même façon que pour une ressource ou un.e auteur.e, Il suffit de **fournir deux fichiers** :

- <span class="meta">index.md</span> contenant **des métadonnées**      
- <span class="meta">featured.jpeg</span>,  une **illustration**  

<br>

<p class="center"> [<img src="figure/zip.png" width="50"/> **Télécharger le modèle**](https://gitlab.huma-num.fr/rzine/add_news/-/raw/master/2020901_actu_name.zip) </p>

<br>
      
Décompressez l'archive, **puis commencez par renommer le répertoire** <span class="meta">2020901_actu_name</span> **de façon explicite**. Utilisez la **date** et des **mots clefs**. *Exemple* :

<br>

<p class="center">
![](figure/actu_name.PNG?style=centerme){width=250px}
</p>


<br>

**Suivez les instructions ci-dessous pour compléter le fichier** <span class="meta">index.md</span>.

<br>


## index.md

Editer le fichier <span class="meta">index.md</span> avec n'importe quel éditeur de texte.

<p class="center">
![](figure/actu.PNG?style=centerme){width=700px} 
</p>

<br>

**Métadonnées obligatoires** :    

- <span class="meta">authors</span> : **Username** de(s) auteur(s
- <span class="meta">title</span> : le **titre**     
- <span class="meta">subtitle</span> : un **sous-titre** 
- <span class="meta">summary</span> : un **résumé court** (phrase d'accroche)  
- <span class="meta">date</span> : la **date** 

**Métadonnée optionnelle :** 

- <span class="meta">tags</span> : une liste de **tags**


<div class="alert alert-danger" role="alert">
**Supprimez les tags** (ou passez en commentaire) **si vous ne souhaitez pas les utiliser**
</div>

<br>


### authors


Indiquez le ou les **username** de(s) auteur(s). Le **username** est déclaré lors du référencement d'un auteur sur rzine.

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
authors:
- jdupont70
```


<div class="alert alert-danger" role="alert">
A. **Vous êtes déjà référencé.e.s comme auteur.e** sur Rzine. **Retrouvez votre username** déclaré dans [**cette liste**](https://gitlab.huma-num.fr/rzine/add_author/-/raw/master/List_username.txt){target="_blank"}      
B. **Vous n'êtes pas encore référencé.e.s** sur Rzine. **Créez votre fiche** auteur en suivant [**ces instructions**](https://rzine.gitpages.huma-num.fr/add_author/){target="_blank"}     
C. **Vous ne souhaitez pas être référencé.e sur Rzine**. Dans ce cas, **indiquez nom et prénom** : </div>   

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
authors:
- Jeanne Dupont
```


En cas de publication collective, **indiquez un.e auteur.e par ligne** :


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
authors:
- jdupont70
- Evangelina Klein
- Rbourdoin81
```


<br>


### title


Indiquez le **titre** (court) de l'actualité.

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
title: 'Formation initiation au GIT'
```

<div class="alert alert-danger" role="alert">
**N'utilisez jamais les caractères ":" et  "'" dans la chaîne de caractère**. Cela empêche le parsage correct des métadonnées. **Cette consigne est à respecter pour toutes les variables !**
</div>

<br>

### subtitle

Indiquez un **sous-titre**.

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
subtitle: 'Institut des systèmes complexes, le 12 décembre 2019'
```

<br>

### summary


Écrivez **une phrase d'accroche**. **170 caractères maximum !**


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
summary: Formation organisée par la FR CIST et l'axe Humanités numériques de l'UMR géographie-cités
```

<br>


### date


Indiquez la **date entre guillemets**  et correctement formatée : **"yyyy-mm-ddThh:mm:ssZ"**

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
date: "2019-09-20T00:00:00Z"
```

<br>


### tags

**Les tags ne sont pas cruciaux pour les actualités**.

```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
tags:
- formation
- git
- CIST
- Géographie-cités
```


<br>


### Texte libre

Ajoutez votre texte [formaté en markdown](https://www.markdownguide.org/basic-syntax/){target="_blank"}, dans le corps du fichier (après l'en-tête qui se termine par "- - -"). Ex : 


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}
---
authors:
- jdupont70

title: 'Formation initiation au GIT'
subtitle: 'Institut des systèmes complexes, le 12 décembre 2019'
summary: Formation organisée par la FR CIST et l'axe Humanités numériques de l'UMR géographie-cités

date: "2019-09-20T00:00:00Z"

tags:
- formation
- git
- CIST
- Géographie-cités

image:
  caption: ''
  focal_point: ""
  placement: 1
  preview_only: true
---


# Title level-1

Rédigé ici votre billet dactualité. [publication](link).
Utilisez la [**syntaxe markdown**](https://www.markdownguide.org/basic-syntax/)pour mettre en forme votre texte.

## Title level-2

Le longueur de ce texte n est pas limité. *Expression libre*.



  
```

<br>


## featured.jpeg


Votre actualité sera illustrée **par défaut**. **Remplacez cette image par celle de votre choix**. *Exemple* :

<p class="center">
![](figure/public1.jpeg?style=centerme){width=230px}
![](figure/arrow2.png?style=centerme){width=50px} 
![](figure/public3.jpg?style=centerme){width=250px} 
</p>


<div class="alert alert-danger" role="alert">
**Votre illustration doit obligatoirement être nommée** <span class="meta">featured</span>. Les formats **jpg**, **jpeg** ou **png** sont acceptés.

**Veillez à ne pas dépasser la dimension** : <span class="meta">1000x1000</span> **pixels**.

</div>

<br>

Pour **ajouter un crédit sur votre illustration**. Utilisez la variable <span class="meta">caption</span> dans le fichier <span class="meta">index.md</span>


```{r  include=TRUE, eval=FALSE, echo=TRUE, tidy=FALSE}

image:
  caption: '© git'
  focal_point: "Center" 
  preview_only: false 
  
```


<br>



## Archive à soumettre

votre répertoire doit comporter **au moins 2 éléments** : 


- **Le fichier** <span class="meta">index.md</span> **complété**
- **Une illustration** nommée <span class="meta">featured.png</span> (ou **.jpg**, **.jpeg**) 


<br>

**Compressez votre répertoire**. **Votre archive est prête !**

<br>


<p class="center">
![](figure/archive.PNG?style=centerme){width=210px}
</p>

<br>

<div class="alert alert-info" role="alert">
**Envoyez l'archive à l'adresse suivante** <span class="meta">**contact@rzine.fr**</span>
</div>